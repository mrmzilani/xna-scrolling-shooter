﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace MonoGame_Shooter
{
    class Player
    {
     
        #region Declarations

        public Sprite playerSprite;
        private float playerSpeed = 300.0f;
        private Rectangle playerAreaLimit;
       
    
        public bool Destroyed = false;

        // controlls bullet start location
        private Vector2 gunOffset = new Vector2(15,5); 
       

        private float shotTimer = 0.0f;
        private float minShotTimer = 0.2f;
        private int playerRadius = 15;
     

        public int playerhealth = 5;
        public int playerScore = 0;



        public TheShots PlayerShots;

        #endregion


        #region constructor

        public Player(Texture2D texture,Rectangle initialFrame,int frameCount,Rectangle screenBounds)
        {
            playerSprite = new Sprite( new Vector2(500, 500),texture, initialFrame,Vector2.Zero);

        

            PlayerShots = new TheShots(texture,new Rectangle(0, 300, 5, 5),4,2,250f,screenBounds);

            //Sets the Screen Bounds so player can not escape from the view
            playerAreaLimit = new Rectangle( 0,0,screenBounds.Width,screenBounds.Height);
       

            for (int x = 1; x < frameCount; x++)
            {
                playerSprite.AddFrame(
                new Rectangle(
                initialFrame.X + (initialFrame.Width * x),
                initialFrame.Y,
                initialFrame.Width,
                initialFrame.Height));
            }
            playerSprite.CollisionRadius = playerRadius;
        }


        #endregion


        #region player input

        private void FireShot()
        {
            if (shotTimer >= minShotTimer)
            {
                PlayerShots.FireShot(
                playerSprite.Location + gunOffset,
                new Vector2(0, -1),
                true);
                shotTimer = 0.0f;
            }
        }


        private void HandleKeyboardInput(KeyboardState keyState, GameTime gameTime)
        {


            if (keyState.IsKeyDown(Keys.Up))
            {
                playerSprite.Velocity += new Vector2(0, -1) ;
            }
            if (keyState.IsKeyDown(Keys.Down))
            {
                playerSprite.Velocity += new Vector2(0, 1);
            }
            if (keyState.IsKeyDown(Keys.Left))
            {
                playerSprite.Velocity += new Vector2(-1, 0) ;
            }
            if (keyState.IsKeyDown(Keys.Right))
            {
                playerSprite.Velocity += new Vector2(1, 0);
            }
            if (keyState.IsKeyDown(Keys.Space))
            {
                FireShot();
             
            }
        }

        #endregion#


        #region update and draw

        private void imposeMovementLimits() //limits ship movements
        {
            Vector2 location = playerSprite.Location;

            if (location.X < playerAreaLimit.X)
                location.X = playerAreaLimit.X;

            if (location.X > (playerAreaLimit.Right - playerSprite.Source.Width)) 
                location.X = (playerAreaLimit.Right - playerSprite.Source.Width);

            if (location.Y < playerAreaLimit.Y)
                location.Y = playerAreaLimit.Y;

            if (location.Y >(playerAreaLimit.Bottom - playerSprite.Source.Height))
                location.Y = (playerAreaLimit.Bottom - playerSprite.Source.Height);

            playerSprite.Location = location;
        }


        public void Update(GameTime gameTime)
        {
            
            PlayerShots.Update(gameTime);
            if (!Destroyed)
            {
                playerSprite.Velocity = Vector2.Zero;
                shotTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                HandleKeyboardInput(Keyboard.GetState(),gameTime);
              
                playerSprite.Velocity.Normalize();
                playerSprite.Velocity *= playerSpeed;
                playerSprite.Update(gameTime);
                imposeMovementLimits();
            }


            if(playerhealth >= 16)
            {
                playerhealth = 16;
            }

        }


        public void Draw(SpriteBatch spriteBatch)
        {
            PlayerShots.Draw(spriteBatch);
            if (!Destroyed)
            {
                playerSprite.Draw(spriteBatch);
            }
        }


        #endregion





    }
}
