﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;

namespace MonoGame_Shooter
{
    public static class SoundManager
    {


        #region declarations

        private static List<SoundEffect> explosions = new List<SoundEffect>(); // list of explosions

        private static int explosionCount = 4;

        private static SoundEffect playerShot;
        private static SoundEffect enemyShot;
        private static SoundEffect coin;
        private static SoundEffect health;

        private static SoundEffect enter;
        private static SoundEffect choose;
        private static SoundEffect pause;

        private static Song menuMusic;
        private static Song gameMusic;

        private static Random rand = new Random();

  
     
  
        #endregion

        #region initilize

        public static void Initialize(ContentManager content)
        {

   

            try
            {
                playerShot = content.Load<SoundEffect>(@"Sounds/Shot1");
                enemyShot = content.Load<SoundEffect>(@"Sounds/Shot2");
                coin = content.Load<SoundEffect>(@"Sounds/CoinSound");
                health = content.Load<SoundEffect>(@"Sounds/healthSound");

                enter = content.Load<SoundEffect>(@"Sounds/menuAccept");
                pause = content.Load<SoundEffect>(@"Sounds/menuBack");
                choose = content.Load<SoundEffect>(@"Sounds/menuSelect");


                menuMusic = content.Load<Song>(@"Sounds/menuMusic");
                gameMusic = content.Load<Song>(@"Sounds/Corneria");

                for (int x = 1; x <= explosionCount; x++)
                {
                    explosions.Add(content.Load<SoundEffect>(@"Sounds\Explosion" + x.ToString())); // adds each sound into list
                }

            }

            catch
            {
                Debug.Write("SoundManager Initialization Failed");
            }
        }

        #endregion

        #region play sounds

        public static void PlayExplosion()
        {
            try
            {
                explosions[rand.Next(0, explosionCount)].Play(); //plays a random explosion each time
            }
            catch
            {
                Debug.Write("PlayExplosion Failed");
            }
        }


        public static void PlayPlayerShot()
        {
            try
            {
                playerShot.Play();
            }
            catch
            {
                Debug.Write("PlayPlayerShot Failed");
            }
        }


        public static void PlayEnemyShot()
        {
            try
            {
                enemyShot.Play(1, rand.Next(0,2),0);
            }
            catch
            {
                Debug.Write("PlayEnemyShot Failed");
            }
        }

        public static void PlayCoin()
        {
            try
            {
                coin.Play(1,1, 0);
            }
            catch
            {
                Debug.Write("coin Failed");
            }
        }


        public static void PlayHealth()
        {
            try
            {
                health.Play(1,0, 0);
            }
            catch
            {
                Debug.Write("coin Failed");
            }
        }


        public static void PlayChoose()
        {
            try
            {
                choose.Play(1, 0, 0);
            }
            catch
            {
                Debug.Write("coin Failed");
            }
        }

        public static void PlayPause()
        {
            try
            {
                pause.Play(1, 0, 0);
            }
            catch
            {
                Debug.Write("coin Failed");
            }
        }


        public static void PlayAccept()
        {
            try
            {
                enter.Play(1, 0, 0);
            }
            catch
            {
                Debug.Write("coin Failed");
            }
        }


        public static void MenuSong()
        {
            try
            {
                MediaPlayer.Pause();
                MediaPlayer.Volume = 0.1f;
                MediaPlayer.Play(menuMusic);
                MediaPlayer.IsRepeating = true;
            }
            catch
            {
                Debug.Write("Music Failed");
            }
        }


        public static void GameSong()
        {
            try
            {
                MediaPlayer.Stop();
                MediaPlayer.Play(gameMusic);
                MediaPlayer.Volume = 0.1f;
                MediaPlayer.IsRepeating = true;
            }
            catch
            {
                Debug.Write("Music Failed");
            }
        }

        public static void StopSong()
        {
            try
            {
                  MediaPlayer.Stop();
      
            }
            catch
            {
                Debug.Write("stop failed Failed");
            }
        }

        #endregion
    }
}
