﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace MonoGame_Shooter
{
    class DifficultySelect : States
    {

        SpriteFont theFont;
        Texture2D MenuScreen;

        public int menuSelect = 0;
        public float keydelay;

        public override void Init(ContentManager Content)
        {

            theFont = Content.Load<SpriteFont>(@"Fonts/MyFont");
            MenuScreen = Content.Load<Texture2D>(@"Backgrounds/MainMenuScreen");


        }
        public override void Update(GameTime gameTime)
        {
            keydelay += (float)gameTime.ElapsedGameTime.TotalSeconds;

            Game1.presentKey = Keyboard.GetState();

            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {

                if(keydelay >=0.1) //Adds a small delay between key presses to stop it from jumping to the last selection
                {
                    menuSelect += 1;
                    keydelay = 0;
                    SoundManager.PlayChoose();
                }

                if (menuSelect > 2)
                {
                    menuSelect = 2;
                }


            }


            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {

                    if (keydelay >= 0.1) //Adds a small delay between key presses to stop it from jumping to the last selection
                     {
                         menuSelect -= 1;
                         keydelay = 0;

                         SoundManager.PlayChoose();
                     }

                    if (menuSelect < 0)
                    {
                        menuSelect = 0;
                    }
                

            }


            switch (menuSelect)
            {
                case 0:

                    if ((Game1.pastKey).IsKeyUp(Keys.Enter) && (Game1.presentKey).IsKeyDown(Keys.Enter)) // Easy
                    {

                        SoundManager.PlayAccept();
                        
                        Play.resetOnce = true;

                        Coin.shortestChance = 40; //less coins
                        Coin.LongestChance = 60;

                        Health.shortestChance = 15; //more health
                        Health.LongestChance = 20;

                        Enemy.shortestChance = 1;
                        Enemy.LongestChance = 3;

                        RedEnemy.shortestChance = 10;  // less dangerous enemies but less points overall
                        RedEnemy.LongestChance = 15;   


                        GoldEnemy.shortestChance = 50;
                        GoldEnemy.LongestChance = 70;


                        Game1.caseChanger = 1;
                    }



                    break;

                case 1:

                    if ((Game1.pastKey).IsKeyUp(Keys.Enter) && (Game1.presentKey).IsKeyDown(Keys.Enter)) // Normal
                    {
                        Play.resetOnce = true;

                        SoundManager.PlayAccept();

                        //medium of everything

                        Coin.shortestChance = 10;
                        Coin.LongestChance = 15;

                        Health.shortestChance = 30;
                        Health.LongestChance = 35;

                        Enemy.shortestChance = 1;
                        Enemy.LongestChance = 3;

                        RedEnemy.shortestChance = 5;
                        RedEnemy.LongestChance = 10;


                        GoldEnemy.shortestChance = 10;
                        GoldEnemy.LongestChance = 20;

     
                        Game1.caseChanger = 1;
                    }

                    break;

                case 2:

                    if ((Game1.pastKey).IsKeyUp(Keys.Enter) && (Game1.presentKey).IsKeyDown(Keys.Enter)) // Hard
                    {
                        Play.resetOnce = true;

                        SoundManager.PlayAccept();

                        Coin.shortestChance = 1; // more coins
                        Coin.LongestChance = 5;

                        Health.shortestChance = 40; //less health
                        Health.LongestChance = 60;

                        Enemy.shortestChance = 1; //more enemies
                        Enemy.LongestChance = 1;

                        RedEnemy.shortestChance = 1;
                        RedEnemy.LongestChance = 1;


                        GoldEnemy.shortestChance = 1; 
                        GoldEnemy.LongestChance = 1;


                        Game1.caseChanger = 1;
                    }


                    break;
            }






            Game1.pastKey = Game1.presentKey;



        }
        public override void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(MenuScreen, new Rectangle(0, 0, 832, 640), Color.White);

            spriteBatch.DrawString(theFont, "Choose Difficulty", new Vector2(50, 330), Color.Red);

            spriteBatch.DrawString(theFont, "Easy", new Vector2(50, 370), Color.White);
            spriteBatch.DrawString(theFont, "Normal", new Vector2(50, 410), Color.White);
            spriteBatch.DrawString(theFont, "Hard", new Vector2(50, 450), Color.White);

            if (menuSelect == 0)
                spriteBatch.DrawString(theFont, "Easy", new Vector2(50, 370), Color.Yellow);
            if (menuSelect == 1)
                spriteBatch.DrawString(theFont, "Normal", new Vector2(50, 410), Color.Yellow);
            if (menuSelect == 2)
                spriteBatch.DrawString(theFont, "Hard", new Vector2(50, 450), Color.Yellow);
        }

    }
}
