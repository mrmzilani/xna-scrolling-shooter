﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace MonoGame_Shooter
{
    public class Play : States 
    {
       
        Texture2D spriteSheet;
        Texture2D playerShip;
        Texture2D shotImage;
        Texture2D healthbar;


        Texture2D background;
        Texture2D background2;
        Texture2D background3;

        Player thePlayer;
        TheShots thePlayerShots;

        Background theBackground;

        EnemyManager theEnemyManager;

        CollisionManager checkCollisions;

        HUD theHUD;

        PowerUpManager powerups;

        ExplosionManager explosions;

        public static bool resetOnce = true;

        public static int showScore;

        public override void Init(ContentManager Content)
        {



            spriteSheet = Content.Load<Texture2D>(@"Explosion/SpriteSheet");

            playerShip = Content.Load<Texture2D>(@"PlayerSprite/ship");

            shotImage = Content.Load<Texture2D>(@"Shot/shot");

            healthbar = Content.Load<Texture2D>(@"HUD/healthbar");
  

            background = Content.Load<Texture2D>(@"Backgrounds/background");
            background2 = Content.Load<Texture2D>(@"Backgrounds/background2");
            background3 = Content.Load<Texture2D>(@"Backgrounds/background3");

        
            thePlayer = new Player(this.playerShip, new Rectangle(0, 0, 64, 64), 4, new Rectangle(0, 0,832,640));
            thePlayerShots = new TheShots(this.shotImage, new Rectangle(0, 0, 32, 32), 1, 5, 500.0f, new Rectangle(0, 0, this.Window.ClientBounds.Width, this.Window.ClientBounds.Height));

             theBackground = new Background(this.background, new Rectangle(0, 0, 832, 640), new Vector2(0, 0), Vector2.Zero,
                                            this.background2, new Rectangle(0, 0, 832, 640), new Vector2(0, -640), Vector2.Zero,
                                            this.background3, new Rectangle(0, 0, 832, 640), new Vector2(0, -1200), Vector2.Zero);


   
             theEnemyManager = new EnemyManager();
             theEnemyManager.init(Content);


            theHUD = new HUD(thePlayer);
            theHUD.init(Content,healthbar);


            powerups = new PowerUpManager();
            powerups.init(Content);

            explosions = new ExplosionManager(spriteSheet, new Rectangle(0, 256, 32, 32), 3, new Rectangle(0,450, 2, 2));

            

            checkCollisions = new CollisionManager(thePlayer, theEnemyManager.Enemies, thePlayerShots, powerups.C, powerups.H,theEnemyManager.RedEnemies,explosions,theEnemyManager.GoldEnemies);

            SoundManager.GameSong();


        }

        public override void Update(GameTime gameTime)
        {

         

            if(resetOnce == true) // Used to reset the whole level when playing again
            {
                resetGame();
                resetOnce = false;
            }


            Game1.presentKey = Keyboard.GetState();
        
            thePlayer.Update(gameTime);
            thePlayerShots.Update(gameTime);

            theEnemyManager.Update(gameTime);

            theBackground.Update(gameTime);

            checkCollisions.Update(gameTime);

            theHUD.Update(gameTime);

            powerups.Update(gameTime);

            explosions.Update(gameTime);

            if ((Game1.pastKey).IsKeyUp(Keys.Enter) && (Game1.presentKey).IsKeyDown(Keys.Enter)) // Pause Game
            {
                Game1.caseChanger = 2;
                SoundManager.PlayPause();
            }

    

            Game1.pastKey = Game1.presentKey;

                
            if(thePlayer.playerhealth <= 0)
            {
              
                showScore = thePlayer.playerScore;
                Game1.caseChanger = 4;
            }


      
    

        }

        public override void Draw(SpriteBatch spriteBatch)
        {

            theBackground.Draw(spriteBatch);

            thePlayer.Draw(spriteBatch);
            thePlayerShots.Draw(spriteBatch);

            theEnemyManager.Draw(spriteBatch);

            theHUD.Draw(spriteBatch);

            powerups.Draw(spriteBatch);

            explosions.Draw(spriteBatch);
           

        }



        public void resetGame()
        {

            SoundManager.GameSong();

            //Clear All Lists
            theEnemyManager.Enemies.theEnemies.Clear();
            theEnemyManager.GoldEnemies.theGoldEnemies.Clear();
            theEnemyManager.RedEnemies.theEnemies.Clear();
            theEnemyManager.RedEnemies.enemyBullets.Shots.Clear();
            thePlayer.PlayerShots.Shots.Clear();
            powerups.C.theCoins.Clear();
            powerups.H.theHealth.Clear();
            explosions.clearExplosions();


            //Move Shots Away
            foreach (Sprite shot in thePlayer.PlayerShots.Shots)
            {
                shot.Location = new Vector2(0, -640);
            }

            foreach (Sprite shot in theEnemyManager.RedEnemies.enemyBullets.Shots)
            {
                shot.Location = new Vector2(0, -640);
            }


           //resets enemy locations
            powerups.ResetLocations();
            theEnemyManager.ResetLocations();


            //Resets the player
            thePlayer.Destroyed = false;
            thePlayer.playerhealth = 5;
            thePlayer.playerScore = 0;
            thePlayer.playerSprite.Location = new Vector2(360, 832);

        }

    }
}
