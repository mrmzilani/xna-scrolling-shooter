﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace MonoGame_Shooter
{
    class GameOver : States
    {

        SpriteFont theFont;
        public int menuSelect = 0;
        Texture2D gameOverScreen;

        public override void Init(ContentManager Content)
        {

            theFont = Content.Load<SpriteFont>(@"Fonts/MyFont");
            gameOverScreen = Content.Load<Texture2D>(@"Backgrounds/pauseScreen");
            
        }
        public override void Update(GameTime gameTime)
        {

            Game1.presentKey = Keyboard.GetState();

            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {

                menuSelect++;
                if (menuSelect > 1)
                    menuSelect = 1;


            }


            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                {

                    menuSelect--;
                    if (menuSelect < 0)
                        menuSelect = 0;
                }

            }









            switch (menuSelect)
            {
                case 0:

                    if ((Game1.pastKey).IsKeyUp(Keys.Enter) && (Game1.presentKey).IsKeyDown(Keys.Enter)) // Play again
                    {
                        Play.resetOnce = true;
                        Game1.caseChanger = 1;
                    }



                    break;

                case 1:

                    if ((Game1.pastKey).IsKeyUp(Keys.Enter) && (Game1.presentKey).IsKeyDown(Keys.Enter)) // main menu
                    {
                        Play.resetOnce = true;
                        Game1.caseChanger = 0;
                    }


                    break;
            }






            Game1.pastKey = Game1.presentKey;



        }
        public override void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(gameOverScreen, new Rectangle(0, 0, 832, 640), Color.White);

            spriteBatch.DrawString(theFont, "Final Score: " + Play.showScore , new Vector2(320, 100), Color.Red);

            spriteBatch.DrawString(theFont, "Play Again", new Vector2(320, 170), Color.White);
            spriteBatch.DrawString(theFont, "Return to Menu", new Vector2(320, 210), Color.White);

            if (menuSelect == 0)
                spriteBatch.DrawString(theFont, "Play Again", new Vector2(320, 170), Color.Yellow);
            if (menuSelect == 1)
                spriteBatch.DrawString(theFont, "Return to Menu", new Vector2(320, 210), Color.Yellow);
        }




    }
}
