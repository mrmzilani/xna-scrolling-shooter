﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;


namespace MonoGame_Shooter
{
    class TitleScreen :States
    {

        SpriteFont theFont;
        Texture2D MenuScreen;
        public int menuSelect = 0;
        public float keydelay;

        public override void Init(ContentManager Content) 
        {
           
            theFont = Content.Load<SpriteFont>(@"Fonts/MyFont");
            MenuScreen = Content.Load<Texture2D>(@"Backgrounds/MainMenuScreen");
     
        }
        public override void Update(GameTime gameTime) 
        {

            keydelay += (float)gameTime.ElapsedGameTime.TotalSeconds;

            SoundManager.StopSong();

            Game1.presentKey = Keyboard.GetState();

            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {

                if (keydelay >= 0.1) //Adds a small delay between key presses to stop it from jumping to the last selection
                {
                    menuSelect += 1;
                    keydelay = 0;
                    SoundManager.PlayChoose();
                }

                if (menuSelect > 1)
                {
                    menuSelect = 1;
                }

            }


            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {

                if (keydelay >= 0.1) //Adds a small delay between key presses to stop it from jumping to the last selection
                {
                    menuSelect -= 1;
                    keydelay = 0;

                    SoundManager.PlayChoose();
                }

                if (menuSelect < 0)
                {
                    menuSelect = 0;
                }

            }

           switch (menuSelect)
           {
               case 0:

                   if ((Game1.pastKey).IsKeyUp(Keys.Enter) && (Game1.presentKey).IsKeyDown(Keys.Enter)) // Select Difficulty
                   {
                       SoundManager.PlayAccept();
                       Game1.caseChanger = 5;
                     
                 
                   }



                   break;

               case 1:

                   if ((Game1.pastKey).IsKeyUp(Keys.Enter) && (Game1.presentKey).IsKeyDown(Keys.Enter) ) // Exits Game
                   {
                       Game1.caseChanger = 3;
                   }


                   break;
           }





            Game1.pastKey = Game1.presentKey;


           
        }
        public override void Draw(SpriteBatch spriteBatch) 
        {

            spriteBatch.Draw(MenuScreen, new Rectangle(0, 0, 832, 640), Color.White);

            spriteBatch.DrawString(theFont, "Play", new Vector2(50, 370), Color.White);
            spriteBatch.DrawString(theFont, "Quit", new Vector2(50, 410), Color.White);

            if (menuSelect == 0)
                spriteBatch.DrawString(theFont, "Play", new Vector2(50, 370), Color.Yellow);
            if (menuSelect == 1)
                spriteBatch.DrawString(theFont, "Quit", new Vector2(50, 410), Color.Yellow);
        }

        

        
    }
}
