﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace MonoGame_Shooter
{
    class Pause : States
    {

        SpriteFont theFont;
        Texture2D pauseScreen;
        Play playGame;

        public int menuSelect = 0;
        public float keydelay;

        public override void Init(ContentManager Content)
        {

            theFont = Content.Load<SpriteFont>(@"Fonts/MyFont");
            pauseScreen = Content.Load<Texture2D>(@"Backgrounds/pauseScreen");
            playGame = new Play();

        }
        public override void Update(GameTime gameTime)
        {

            keydelay += (float)gameTime.ElapsedGameTime.TotalSeconds;

            Game1.presentKey = Keyboard.GetState();

            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {

                if (keydelay >= 0.1) //Adds a small delay between key presses to stop it from jumping to the last selection
                {
                    menuSelect += 1;
                    keydelay = 0;
                    SoundManager.PlayChoose();
                }

                if (menuSelect > 1)
                {
                    menuSelect = 1;
                }


            }


            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {

                if (keydelay >= 0.1) //Adds a small delay between key presses to stop it from jumping to the last selection
                {
                    menuSelect -= 1;
                    keydelay = 0;

                    SoundManager.PlayChoose();
                }

                if (menuSelect < 0)
                {
                    menuSelect = 0;
                }

            }


            switch(menuSelect)
            {
                case 0:

                if ((Game1.pastKey).IsKeyUp(Keys.Enter) && (Game1.presentKey).IsKeyDown(Keys.Enter))// Resume Game
                {
                    SoundManager.PlayAccept();
                    Game1.caseChanger = 1;
                 }


                break;

                case 1:

                if ((Game1.pastKey).IsKeyUp(Keys.Enter) && (Game1.presentKey).IsKeyDown(Keys.Enter)) // Title Screen
                {
                    SoundManager.PlayAccept();
                    Play.resetOnce = true;
                    Game1.caseChanger = 0;
                  
                     
                }


                 break;
            }





            Game1.pastKey = Game1.presentKey;





        }
        public override void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(pauseScreen, new Rectangle(0, 0, 832, 640), Color.White);

            spriteBatch.DrawString(theFont, "PAUSED", new Vector2(320, 150), Color.Red);
            spriteBatch.DrawString(theFont, "Resume Game", new Vector2(320, 170), Color.White);
            spriteBatch.DrawString(theFont, "Return To Menu", new Vector2(320, 210), Color.White);

            if (menuSelect == 0)
                spriteBatch.DrawString(theFont, "Resume Game", new Vector2(320, 170), Color.Yellow);
            if (menuSelect == 1)
                spriteBatch.DrawString(theFont, "Return To Menu", new Vector2(320, 210), Color.Yellow);

        }
    }
}
