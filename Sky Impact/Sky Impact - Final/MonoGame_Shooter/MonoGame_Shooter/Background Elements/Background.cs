﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGame_Shooter
{
    class Background 
    {

        #region Declarations
      
        public Sprite backgroundImage;
        public Sprite backgroundImage2;
        public Sprite backgroundImage3;

        #endregion

        #region Constructor

        public Background(Texture2D image, Rectangle imageArea,Vector2 startPosition, Vector2 Scrolling,
                          Texture2D image2, Rectangle imageArea2,Vector2 startPosition2, Vector2 Scrolling2,
                          Texture2D image3, Rectangle imageArea3,Vector2 startPosition3, Vector2 Scrolling3)
        {

            backgroundImage = new Sprite(startPosition, image, imageArea, Scrolling);
            backgroundImage2 = new Sprite(startPosition2, image2, imageArea2, Scrolling2);
            backgroundImage3 = new Sprite(startPosition3, image3, imageArea3, Scrolling3);

        }

        #endregion

        #region Update

        public void Update(GameTime gameTime)
        {

            backgroundImage.Location += new Vector2(0,2);
            backgroundImage2.Location += new Vector2(0,2);
            backgroundImage3.Location += new Vector2(0,2);


            if(backgroundImage.Location.Y > 800)
            {
                backgroundImage.Location = new Vector2(0,-700);
            }

            if (backgroundImage2.Location.Y > 800)
            {
                backgroundImage2.Location = new Vector2(0, -700);
            }


            if (backgroundImage3.Location.Y > 800)
            {
                backgroundImage3.Location = new Vector2(0, -700);
            }



        }

        #endregion

        #region Draw

        public void Draw(SpriteBatch spriteBatch)
        {

            backgroundImage.Draw(spriteBatch);
            backgroundImage2.Draw(spriteBatch);
            backgroundImage3.Draw(spriteBatch);

        }



        #endregion


    }
}
