﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;




namespace MonoGame_Shooter
{
    class HUD
    {

        SpriteFont theFont;
        Player Playerinfo;



        Rectangle hpRectangle;
        Texture2D tex;

        GraphicsDevice g;

        public HUD(Player P)
        {
        
            this.Playerinfo = P; 
        }



        public void init(ContentManager Content, Texture2D healthTexture)
        {

           theFont = Content.Load<SpriteFont>(@"Fonts/MyFont");

           hpRectangle = new Rectangle(0, 10, 0,25);

           this.tex = healthTexture;


        }

        public void Update(GameTime gametime)
        {


            hpRectangle.Width = Playerinfo.playerhealth * 50;

        }

        public void Draw(SpriteBatch spriteBatch)
        {


            spriteBatch.Draw(tex, hpRectangle, Color.Red);
            

            spriteBatch.DrawString(theFont, "Health:" + Playerinfo.playerhealth, new Vector2(0, 10), Color.White);
            spriteBatch.DrawString(theFont, "Score :" + Playerinfo.playerScore, new Vector2(0, 40), Color.White);
         

        }


    }
}
