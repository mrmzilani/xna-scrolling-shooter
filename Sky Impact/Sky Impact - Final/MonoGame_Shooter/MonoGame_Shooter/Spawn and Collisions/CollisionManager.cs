﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace MonoGame_Shooter
{
    class CollisionManager
    {

        private Player P;
        private Enemy E;
        private TheShots S;
        private RedEnemy RE;
        private Coin C;
        private Health H;
        private ExplosionManager EX;
        private GoldEnemy GE;

      public CollisionManager(Player thePlayer, Enemy theEnemy,TheShots Shots, Coin theCoin, Health theHealth, RedEnemy theRedEnemy, ExplosionManager theExplosions,GoldEnemy theGoldEnemy)
      {

            this.P = thePlayer;
            this.E = theEnemy;
            this.S = Shots;
            this.C = theCoin;
            this.H = theHealth;
            this.RE = theRedEnemy;
            this.EX = theExplosions;
            this.GE = theGoldEnemy;

       }

        


    
        public void Update(GameTime gameTime)
        {
           
            foreach(Coin coin in C.theCoins) // Player to coin colisions
            {

                if (coin.CoinSprite.IsBoxColliding(P.playerSprite.BoundingBoxRect))
                {
                    SoundManager.PlayCoin();

                    coin.Destroyed = true;
                    coin.CoinSprite.Location = new Vector2(-200, 0);
                    P.playerScore += 100;
                    

                }

            }


            foreach(Health health in H.theHealth) //player to health collisions
            {

                if (health.healthSprite.IsBoxColliding(P.playerSprite.BoundingBoxRect))
                {

                    SoundManager.PlayHealth();

                    health.healthSprite.Location = new Vector2(-200, 0);

                    if(P.playerhealth >= 16) //if at max health
                    {
                        P.playerScore += 50; //give score instead
                    }
                    else
                    {
                        P.playerhealth += 1;
                    }

                }


            }


            foreach(Enemy En in E.theEnemies)  // player to enemy
            {

     
                    if (En.enemySprite.IsBoxColliding(P.playerSprite.BoundingBoxRect))
                    {
                        
                        EX.AddExplosion(En.enemySprite.Center,
                           En.enemySprite.Velocity / 10);

                        SoundManager.PlayExplosion();
                     

                        En.Destroyed = true;
                        En.enemySprite.Location = new Vector2(0,640);

                        P.playerhealth = P.playerhealth - 1; //(deals light damage)
                        P.playerScore += 50;

                        if (P.playerhealth <= 0)
                        {
                            P.Destroyed = true;
                        
                           
                        }

                }

            }


            foreach (Enemy GEn in GE.theGoldEnemies)  // player to gold enemy
            {


                if (GEn.enemySprite.IsBoxColliding(P.playerSprite.BoundingBoxRect))
                {

                    EX.AddExplosion(GEn.enemySprite.Center,
                       GEn.enemySprite.Velocity / 10);

                    SoundManager.PlayExplosion();

                    GEn.Destroyed = true;
                    GEn.enemySprite.Location = new Vector2(0, 640);

                    P.playerhealth = P.playerhealth - 4; //(more dangerous to crash into)
                    P.playerScore += 100;

                    if (P.playerhealth <= 0)
                    {
                        P.Destroyed = true;


                    }

                }

            }




            foreach (Enemy RedE in RE.theEnemies )  // player to red enemy
            {


                if (RedE.enemySprite.IsBoxColliding(P.playerSprite.BoundingBoxRect))
                {

                    EX.AddExplosion(RedE.enemySprite.Center,
                       RedE.enemySprite.Velocity / 10);

                    SoundManager.PlayExplosion();
                   
                    RedE.Destroyed = true;
                    RedE.enemySprite.Location = new Vector2(0, 640);

                    P.playerhealth = P.playerhealth - 2; //(deals more damage on crash)
                    P.playerScore += 50;

                    if (P.playerhealth <= 0)
                    {
                        P.Destroyed = true;


                    }



                }

            }




            foreach (Sprite shot in P.PlayerShots.Shots)  //playershot to enemy
            {

                foreach (Enemy enemy in E.theEnemies)
                {


                    if(shot.IsBoxColliding(enemy.enemySprite.BoundingBoxRect))
                    {

                        EX.AddExplosion(enemy.enemySprite.Center,
                                   enemy.enemySprite.Velocity / 10);

                        SoundManager.PlayExplosion();

                        enemy.Destroyed = true;
                        shot.Location = new Vector2(-200, 0);

                        enemy.enemySprite.Location = new Vector2(-200,0);
                        

                        P.playerScore += 100;

                      
                        
                       


                        for (int x = 0; x < enemy.theEnemies.Count; x++)
                        {
                            enemy.theEnemies.RemoveAt(x);
                        }

                      
                    }

                }

            }



            foreach (Sprite shot in P.PlayerShots.Shots)  //playershot to red enemy
            {

                foreach (Enemy enemy in RE.theEnemies)
                {


                    if (shot.IsBoxColliding(enemy.enemySprite.BoundingBoxRect))
                    {

                        EX.AddExplosion(enemy.enemySprite.Center,
                                   enemy.enemySprite.Velocity / 10);

                        SoundManager.PlayExplosion();

                        enemy.Destroyed = true;
                        shot.Location = new Vector2(-200, 0);

                        enemy.enemySprite.Location = new Vector2(-200, 0);


                        P.playerScore += 100;


                        for (int x = 0; x < enemy.theEnemies.Count; x++)
                        {
                            enemy.theEnemies.RemoveAt(x);
                        }


                    }

                }

            }



            foreach (Sprite shot in P.PlayerShots.Shots)  //playershot to goldenemy
            {

                foreach (Enemy enemy in GE.theGoldEnemies)
                {


                    if (shot.IsBoxColliding(enemy.enemySprite.BoundingBoxRect))
                    {

                        EX.AddExplosion(enemy.enemySprite.Center,
                                   enemy.enemySprite.Velocity / 10);

                        SoundManager.PlayExplosion();

                        enemy.Destroyed = true;
                        shot.Location = new Vector2(-200, 0);

                        enemy.enemySprite.Location = new Vector2(-200, 0);


                        P.playerScore += 500; //(gold enemy has more value)


                        for (int x = 0; x < enemy.theEnemies.Count; x++)
                        {
                            enemy.theEnemies.RemoveAt(x);
                        }


                    }

                }

            }



            foreach (Sprite Eshot in RE.enemyBullets.Shots)  //Enemy shot to Player
            {

                foreach (Enemy RedE in RE.theEnemies)
                {


                    if (Eshot.IsBoxColliding(P.playerSprite.BoundingBoxRect))
                    {

                        EX.AddExplosion(P.playerSprite.Center,
                        P.playerSprite.Velocity / 10);

                        SoundManager.PlayExplosion();
                       
                        P.playerhealth = P.playerhealth - 1;
                        Eshot.Location  = new Vector2(-200,0);

                        if (P.playerhealth <= 0)
                        {
                            P.Destroyed = true;


                        }

                    }

                }

            }
         
      
           




     


        }
               
          

    }
}
