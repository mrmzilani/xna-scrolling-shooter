﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoGame_Shooter
{
    class SpawnPointManager
    {

        public List<int> SquaresX = new List<int>();


        public List<int> SquaresY = new List<int>();

        int spriteSizeX = 0;
        int spriteSizeY = 0;

        bool firstTime = true;

        Random rand = new Random();


        public void init()
        {
   
            for (int x = 0; x < 14; x++)
            {
                
                SquaresX.Insert(x, spriteSizeX);

                spriteSizeX += 64;

            }

            for (int y = 0; y < 10; y++)
            {

                SquaresY.Insert(y, spriteSizeY);

                spriteSizeY -= 64;

            }


  


        }


        public int SpawnX()
        {

            int val = 0;  
            val = rand.Next(0, 14);
            return SquaresX.ElementAt(val); 
        }


        public int SpawnY()
        {

            int val = 0;  //create int
            int lastVal = 0;

            if (firstTime == true) //do if first time
            {
                val = rand.Next(0, 10);
                lastVal = val;
                firstTime = false;
                return SquaresY.ElementAt(val);
            }

            else
            {
                do
                {
                    val = rand.Next(0, 10); //random value
                }
                
                while (val == lastVal); //if value is the same as last value then it will randomise again


                return SquaresY.ElementAt(val); //then return the value

            }

            //(this code does not fully work as enemies are still spawing in the same place

        }



        public void clearLocations()
        {


            spriteSizeX =  0;

            for (int x = 0; x < 14; x++)
            {

                SquaresX.Insert(x, spriteSizeX);

                spriteSizeX += 64;

                
            }



        }


    }
}
