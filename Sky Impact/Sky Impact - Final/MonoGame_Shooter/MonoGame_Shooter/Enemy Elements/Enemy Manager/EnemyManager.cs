﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;


namespace MonoGame_Shooter
{
    class EnemyManager
    {

      public Enemy Enemies;
      public RedEnemy RedEnemies;
      public GoldEnemy GoldEnemies;

        Texture2D enemy;
        Texture2D redEnemy;
        Texture2D GoldEnemy;
        Texture2D shotImage;

    

        public void init(ContentManager content)
        {

            enemy = content.Load<Texture2D>(@"EnemySprites/enemy");
            redEnemy = content.Load<Texture2D>(@"EnemySprites/enemy2");
            GoldEnemy = content.Load<Texture2D>(@"EnemySprites/enemy3");
            shotImage = content.Load<Texture2D>(@"Shot/enemyshot");

            Enemies = new Enemy(this.enemy, new Rectangle(0, 0, 64, 64),3);
            Enemies.initaliseEnemies();

            RedEnemies = new RedEnemy(this.redEnemy, new Rectangle(0, 0, 64, 64), 1, shotImage, new Rectangle(0, 0, 832, 640));
            RedEnemies.initaliseEnemies();

            GoldEnemies = new GoldEnemy(this.GoldEnemy, new Rectangle(0, 0, 64, 64), 1);
            GoldEnemies.initaliseEnemies();
            


        }



        public void Update(GameTime gameTime)
        {
            Enemies.Update(gameTime);
            RedEnemies.Update(gameTime);
            GoldEnemies.Update(gameTime);

           
        }


        public void Draw(SpriteBatch spriteBatch)
        {

            Enemies.Draw(spriteBatch);
            RedEnemies.Draw(spriteBatch);
            GoldEnemies.Draw(spriteBatch);
          

        }


        public void ResetLocations()
        {

            foreach (Enemy En in Enemies.theEnemies)
            {
                En.Destroyed = true;
                En.enemySprite.Location = new Vector2(0, 640);

            }

            foreach (Enemy En in RedEnemies.theEnemies)
            {
                En.Destroyed = true;
                En.enemySprite.Location = new Vector2(0, 640);

            }

        }


    }
}
