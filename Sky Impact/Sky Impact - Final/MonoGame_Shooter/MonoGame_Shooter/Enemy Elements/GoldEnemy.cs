﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace MonoGame_Shooter
{
    class GoldEnemy
    {

               
        #region Declarations


        public Sprite GoldenemySprite;
    
        public bool Destroyed = false;
        public int enemyhealth = 10;

        public List<Enemy> theGoldEnemies = new List<Enemy>();

        Texture2D Texture;
        Rectangle InitalFrame;
        int Frames;

        Random rand = new Random();
        public int chance = 0;
        public static int shortestChance = 30;
        public static int LongestChance = 35;

        Enemy thisEnemy;

        SpawnPointManager SpawnHere = new SpawnPointManager();

        public bool isVisable;
       
        public int maxAmount = 10;
        public float Respawn;

        #endregion

        #region Constructor


        public GoldEnemy(Texture2D texture, Rectangle initialFrame, int frameCount)
        {

            GoldenemySprite = new Sprite(new Vector2(0,0),texture,initialFrame, Vector2.Zero);

            this.InitalFrame = initialFrame;
            this.Texture = texture;
            this.Frames = frameCount;

            for (int x = 1; x < frameCount; x++)
            {
                GoldenemySprite.AddFrame(
                new Rectangle(
                initialFrame.X + (initialFrame.Width * x),
                initialFrame.Y,
                initialFrame.Width,
                initialFrame.Height));
            }


            SpawnHere.init();



        }

        public void addEnemy()
        {


             thisEnemy = new Enemy(Texture, InitalFrame, Frames);

             thisEnemy.enemySprite.Location = new Vector2(SpawnHere.SpawnX(),SpawnHere.SpawnY());

             theGoldEnemies.Add(thisEnemy);


        }


        public void initaliseEnemies()
        {
           
            addEnemy();

        }


        #endregion

        #region Update

        public void SpawnEnemy()
        {

            chance = rand.Next(shortestChance, LongestChance);

            if(Respawn >=chance)
            {
                Respawn = 0;

                 addEnemy();

            }


        }


        public void Update(GameTime gameTime)
        {


            Respawn += (float)gameTime.ElapsedGameTime.TotalSeconds;


            SpawnEnemy();

            foreach (Enemy En in theGoldEnemies)   //REMOVES OFF SCREEN ENEMIES
            {
               if( En.enemySprite.Location.Y > 640)
               {

                   isVisable = false;
                 
               }

               if (En.isVisable == false)
               {
                   for (int x = 0; x < En.theEnemies.Count; x++)
                   {
                       En.theEnemies.RemoveAt(x);
                   }
               }

            }


            foreach (Enemy En in theGoldEnemies) //Move Enemies
            {
                En.enemySprite.Location += new Vector2(0,9);

            }
   
          
        }

        #endregion

        #region Draw

        public void Draw(SpriteBatch spriteBatch)
        {

           /* if(!Destroyed)
            {
             
                enemySprite.Draw(spriteBatch);

            }*/


            foreach (Enemy En in theGoldEnemies)
            {
                if (En.Destroyed == false)
                {

                    En.enemySprite.Draw(spriteBatch);

                }

            }

        }



        #endregion


    }
}
