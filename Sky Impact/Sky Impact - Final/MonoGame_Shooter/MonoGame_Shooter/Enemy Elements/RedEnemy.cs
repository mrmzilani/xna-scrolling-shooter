﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Microsoft.Xna.Framework.Input;

//http://www.youtube.com/watch?v=tfiKwOo_4xo

namespace MonoGame_Shooter
{
    class RedEnemy
    {

        #region Declarations


        public Sprite enemySprite;
        public bool Destroyed = false;
        public int enemyhealth = 10;

        public List<Enemy> theEnemies = new List<Enemy>();

        Texture2D Texture;
        Rectangle InitalFrame;
        int Frames;

        Random rand = new Random();
        public int chance = 0;
        public static int shortestChance = 30;
        public static int LongestChance = 35;


        Enemy thisEnemy;

        SpawnPointManager SpawnHere = new SpawnPointManager();

        public bool isVisable;
       
        public int maxAmount = 10;
        public float Respawn;

        public EnemyShots enemyBullets;
        public float bulletDelay;


        #endregion

        #region Constructor


        public RedEnemy(Texture2D texture, Rectangle initialFrame, int frameCount ,Texture2D bullet, Rectangle ScreenBounds )
        {

            enemySprite = new Sprite(new Vector2(0,0),texture,initialFrame, Vector2.Zero);

            this.InitalFrame = initialFrame;
            this.Texture = texture;
            this.Frames = frameCount;

            for (int x = 1; x < frameCount; x++)
            {
                enemySprite.AddFrame(
                new Rectangle(
                initialFrame.X + (initialFrame.Width * x),
                initialFrame.Y,
                initialFrame.Width,
                initialFrame.Height));
            }


            SpawnHere.init();


         
            enemyBullets = new EnemyShots(bullet,new Rectangle(0,0,32,32),1,2,250f,ScreenBounds);


        }

        public void addEnemy()
        {


              thisEnemy = new Enemy(Texture, InitalFrame, Frames);

              thisEnemy.enemySprite.Location = new Vector2(SpawnHere.SpawnX(),SpawnHere.SpawnY());

              enemyBullets.FireShot(thisEnemy.enemySprite.Location, new Vector2(0, 1), false);

              SoundManager.PlayEnemyShot();
  
              theEnemies.Add(thisEnemy);


        }


 

        public void initaliseEnemies()
        {
           
            addEnemy();

        }


        #endregion

        #region Update

        public void SpawnEnemy()
        {

            chance = rand.Next(shortestChance, LongestChance);

            if(Respawn >=chance)
            {
                Respawn = 0;

                 addEnemy();
                

            }


        }

        public void Update(GameTime gameTime)
        {


            Respawn += (float)gameTime.ElapsedGameTime.TotalSeconds;
            bulletDelay += (float)gameTime.ElapsedGameTime.TotalSeconds;

            SpawnEnemy();

            foreach (Enemy En in theEnemies)   //REMOVES OFF SCREEN ENEMIES
            {
               if( En.enemySprite.Location.Y > 640)
               {

                   isVisable = false;
                 
               }

               if (En.isVisable == false)
               {
                   for (int x = 0; x < En.theEnemies.Count; x++)
                   {
                       En.theEnemies.RemoveAt(x);
                   }
               }

            }



            foreach (Enemy En in theEnemies)
            {
                En.enemySprite.Location += new Vector2(0,6);

            }


            foreach(Sprite enemyshot in enemyBullets.Shots)  //removes enemy bullets off screen
            {
                if(enemyBullets.thisShot.Location.Y >= 640)
                {

                    for(int x = 0 ; x < enemyBullets.Shots.Count;x++)
                    {

                        enemyBullets.Shots.RemoveAt(x);

                    }

                }
                


            }
                        


            enemyBullets.Update(gameTime);
      
          
        }


        #endregion

        #region Draw

        public void Draw(SpriteBatch spriteBatch)
        {

            enemyBullets.Draw(spriteBatch);


            foreach (Enemy En in theEnemies)
            {
                if (En.Destroyed == false)
                {

                    En.enemySprite.Draw(spriteBatch);
                
                   

                }

            }

        }



        #endregion



    }
}
