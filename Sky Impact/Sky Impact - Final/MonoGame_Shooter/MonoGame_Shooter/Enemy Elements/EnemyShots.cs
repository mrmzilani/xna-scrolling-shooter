﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGame_Shooter
{
    class EnemyShots
    {
                
        #region declarations

        public List<Sprite> Shots = new List<Sprite>();
        private Rectangle screenBounds;
        private static Texture2D Texture;
        private static Rectangle InitialFrame;
        private static int FrameCount;
        private float shotSpeed;
        private static int CollisionRadius;

        public Sprite thisShot; 
  

        #endregion

        #region constructor

        public EnemyShots(Texture2D texture,Rectangle initialFrame,int frameCount,int collisionRadius,
                           float shotSpeed,Rectangle screenBounds)
        {
            Texture = texture;
            InitialFrame = initialFrame;
            FrameCount = frameCount;
            CollisionRadius = collisionRadius;
            this.shotSpeed = shotSpeed;
            this.screenBounds = screenBounds;
        }


        #endregion

        #region fire shots

        public void FireShot(Vector2 location,Vector2 velocity, bool playerFired)
        {
            thisShot = new Sprite(location,Texture,InitialFrame,velocity);

            thisShot.Velocity *= shotSpeed * 2;
            
            for (int x = 1; x < FrameCount; x++)
            {
                thisShot.AddFrame(new Rectangle(
                InitialFrame.X + (InitialFrame.Width * x),
                InitialFrame.Y,
                InitialFrame.Width,
                InitialFrame.Height));
            }

            thisShot.CollisionRadius = CollisionRadius;


            Shots.Add(thisShot);

  
        }


        #endregion

        #region update and draw shots

        public void Update(GameTime gameTime)
        {
            for (int x = Shots.Count - 1; x >= 0; x--)
            {
                Shots[x].Update(gameTime);
                if (Shots[x].Location.Y > 640)
                {
                    Shots.RemoveAt(x);
                }
            }



        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Sprite shot in Shots)
            {
                shot.Draw(spriteBatch);
            }
        }


        #endregion
    }
}
