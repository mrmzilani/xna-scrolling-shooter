﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace MonoGame_Shooter
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        TitleScreen titleScreen;
        DifficultySelect difficultySelect;
        Play playGame;
        Pause pauseGame;
        GameOver gameover;
        

        public static int caseChanger = 0;
        
        public static KeyboardState presentKey, pastKey;

        enum theState {TitleScreen,difficultySelect,Play,Pause,GameOver};
        theState State = theState.TitleScreen;


        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            this.graphics.PreferredBackBufferWidth = 832;
            this.graphics.PreferredBackBufferHeight = 640;
            this.graphics.ApplyChanges();
        
            this.IsMouseVisible = true;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
           spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

           titleScreen = new TitleScreen();
           titleScreen.Init(Content);

           playGame = new Play();
           playGame.Init(Content);

           pauseGame = new Pause();
           pauseGame.Init(Content);

           gameover = new GameOver();
           gameover.Init(Content);

           difficultySelect = new DifficultySelect();
           difficultySelect.Init(Content);

           SoundManager.Initialize(Content);



          

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
           

            presentKey = Keyboard.GetState();

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            // 0 = title
            // 1 = play
            // 2 = pause
            // 3 = exit
            // 4 = Gameover
            // 5 = Choose Difficulty
    

            if (caseChanger == 0)
            {
               
                State = theState.TitleScreen;

            }

            if (caseChanger == 1)
            {

                
                State = theState.Play;
            }

            if (caseChanger == 2)
            {

                State = theState.Pause;
            }

            if (caseChanger == 3)
            {

                this.Exit();
            }

            if (caseChanger == 4)
            {

                State = theState.GameOver;
            }

            if (caseChanger == 5)
            {

                State = theState.difficultySelect;
            }



            switch (State)
            {
                case theState.TitleScreen:

                    titleScreen.Update(gameTime);

                    break;

                case theState.Play:
                 
                    playGame.Update(gameTime);

                    break;

                case theState.Pause:

                    pauseGame.Update(gameTime);

                    break;

                case theState.GameOver:

                    gameover.Update(gameTime);

                    break;


                case theState.difficultySelect:

                    difficultySelect.Update(gameTime);

                    break;

                    

            }




            pastKey = presentKey;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime) //gametime why?
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();


            switch (State)
            {
                case theState.TitleScreen:
         
                   titleScreen.Draw(spriteBatch);

                    break;

                case theState.Play:

           
                    playGame.Draw(spriteBatch);

                    break;

                case theState.Pause:

                    pauseGame.Draw(spriteBatch);

                    break;


                case theState.GameOver:

                    gameover.Draw(spriteBatch);

                    break;


                case theState.difficultySelect:

                    difficultySelect.Draw(spriteBatch);

                    break;

            }
        

            spriteBatch.End();

          



            base.Draw(gameTime);
        }
    }
}
