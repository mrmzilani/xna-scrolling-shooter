﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace MonoGame_Shooter
{
    class Health
    {

        #region Declarations

        Health thisHealth;
        public Sprite healthSprite;

        public List<Health> theHealth = new List<Health>();

        Texture2D Texture;
        Rectangle InitalFrame;
        int Frames;

        public bool Destroyed = false;
        public bool isVisable;


        public int maxAmount = 10;
        public float Respawn;

        Random rand = new Random();
        public int chance = 0;
        public static int shortestChance = 30;
        public static int LongestChance = 35;

        SpawnPointManager spawn = new SpawnPointManager();

        #endregion

        #region Constructor


        public Health(Texture2D texture, Rectangle initialFrame, int frameCount)
        {

            healthSprite = new Sprite(new Vector2(0, 0), texture, initialFrame, Vector2.Zero);

            this.InitalFrame = initialFrame;
            this.Texture = texture;
            this.Frames = frameCount;

            for (int x = 1; x < frameCount; x++)
            {
                healthSprite.AddFrame(
                new Rectangle(
                initialFrame.X + (initialFrame.Width * x),
                initialFrame.Y,
                initialFrame.Width,
                initialFrame.Height));
            }

            spawn.init();

        }

        public void addPowerUp()
        {


            thisHealth = new Health(Texture, InitalFrame, Frames);

            thisHealth.healthSprite.Location = new Vector2(spawn.SpawnX(), spawn.SpawnY());

            thisHealth.isVisable = true;

            theHealth.Add(thisHealth);


        }


        public void init()
        {

            addPowerUp();

        }


        #endregion

        #region Update

        public void Spawn()
        {

            chance = rand.Next(shortestChance,LongestChance);

            if (Respawn >= chance)
            {
                Respawn = 0;

                addPowerUp();

            }


        }

        public void reset()
        {
            foreach (Health H in theHealth) //resets the location
            {
                H.Destroyed = true;
                H.healthSprite.Location = new Vector2(0, 640);

            }
        }


        public void Update(GameTime gameTime)
        {


            Respawn += (float)gameTime.ElapsedGameTime.TotalSeconds;


            Spawn();

            foreach (Health H in theHealth)
            {
                if (H.healthSprite.Location.Y > 640)
                {

                    for (int x = 0; x < H.theHealth.Count; x++)
                    {
                        H.theHealth.RemoveAt(x);
                        H.Destroyed = true;

                    }

                }

            }


            foreach (Health H in theHealth)//Move 
            {
                H.healthSprite.Location += new Vector2(0, 4);

            }

        }

        #endregion

        #region Draw

        public void Draw(SpriteBatch spriteBatch)
        {


            foreach (Health H in theHealth)
            {
                if (H.Destroyed == false)
                {

                    H.healthSprite.Draw(spriteBatch);

                }

            }

        }



        #endregion


    }
}
