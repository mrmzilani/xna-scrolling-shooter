﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace MonoGame_Shooter
{
    class PowerUpManager
    {

        public Coin C;
        public Health H;
    

        public void init(ContentManager content)
        {

            C = new Coin(content.Load<Texture2D>(@"PowerUp/Coin"), new Rectangle(0, 0, 64, 64), 1);

            H = new Health(content.Load<Texture2D>(@"PowerUp/health"), new Rectangle(0, 0, 64, 64), 1);
            

        }



        public void Update(GameTime gameTime)
        {

            C.Update(gameTime);
            H.Update(gameTime);
         

        }


        public void Draw(SpriteBatch spriteBatch)
        {
            C.Draw(spriteBatch);
            H.Draw(spriteBatch);

        }

        public void ResetLocations()
        {
            C.reset();
            H.reset();
        }

    }
}
