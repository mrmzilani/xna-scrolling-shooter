﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace MonoGame_Shooter
{
    class Coin 
    {
                
        #region Declarations

        Coin thisCoin;
        public Sprite CoinSprite;

        public List<Coin> theCoins = new List<Coin>();

        Texture2D Texture;
        Rectangle InitalFrame;
        int Frames;

        public bool Destroyed = false;
        public bool isVisable;
    
       
        public int maxAmount = 10;
        public float Respawn;

        Random rand = new Random();
        public int chance = 0;
        public static int shortestChance = 30;
        public static int LongestChance = 60;

        SpawnPointManager spawn = new SpawnPointManager();

        #endregion

        #region Constructor


        public Coin(Texture2D texture, Rectangle initialFrame, int frameCount)
        {

            CoinSprite = new Sprite(new Vector2(0,0),texture,initialFrame, Vector2.Zero);

            this.InitalFrame = initialFrame;
            this.Texture = texture;
            this.Frames = frameCount;

            for (int x = 1; x < frameCount; x++)
            {
                CoinSprite.AddFrame(
                new Rectangle(
                initialFrame.X + (initialFrame.Width * x),
                initialFrame.Y,
                initialFrame.Width,
                initialFrame.Height));
            }

            spawn.init();

        }

        public void addPowerUp()
        {


              thisCoin = new Coin(Texture, InitalFrame, Frames);

              thisCoin.CoinSprite.Location = new Vector2(spawn.SpawnX(),spawn.SpawnY());

              thisCoin.isVisable = true;

              theCoins.Add(thisCoin);


        }


        public void init()
        {
           
            addPowerUp();

        }


        #endregion

        #region Update

        public void Spawn()
        {

           chance = rand.Next(shortestChance,LongestChance);

            if (Respawn >= chance) 
            {
                Respawn = 0;

                addPowerUp();

            }


        }

        public void reset()
        {
            foreach (Coin C in theCoins) //resets the location
            {
                C.Destroyed = true;
                C.CoinSprite.Location = new Vector2(0, 640);

            }
        }


        public void Update(GameTime gameTime)
        {


            Respawn += (float)gameTime.ElapsedGameTime.TotalSeconds;


            Spawn();

            foreach (Coin C in theCoins)  
            {
               if( C.CoinSprite.Location.Y > 640)
               {

                   for (int x = 0; x < C.theCoins.Count; x++)
                   {
                       C.theCoins.RemoveAt(x);
                       C.Destroyed = true;

                   }
                 
               }

            }


            foreach (Coin C in theCoins) //Move 
            {
                C.CoinSprite.Location += new Vector2(0,4);

            }
          
        }

        #endregion

        #region Draw

        public void Draw(SpriteBatch spriteBatch)
        {

   
            foreach (Coin C in theCoins) 
            {
                if (C.Destroyed == false)
                {

                    C.CoinSprite.Draw(spriteBatch);

                }

            }

        }



        #endregion

      
    }
}
